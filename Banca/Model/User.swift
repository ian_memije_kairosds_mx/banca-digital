//
//  User.swift
//  Banca

struct User {
    var id: String
    var email: String
    var firstName: String
    var lastName: String
    var password: String
    var location: String
    var iat: Int
    var exp: Int
    var token: String
    var cards: [Card]
    var cardsInfo: [CardInfo]
    
    init(location: String, token: String, cards: [Card], cardsInfo: [CardInfo], id: String, iat: Int, exp: Int, dictionary: [String : Any]) {
        self.id = id
        self.email = dictionary["email"] as? String ?? ""
        self.firstName = dictionary["firstName"] as? String ?? ""
        self.lastName = dictionary["lastName"] as? String ?? ""
        self.password = dictionary["password"] as? String ?? ""
        self.iat = iat
        self.exp = exp
        self.token = token
        self.cards = cards
        self.cardsInfo = cardsInfo
        self.location = location
    }
}
