//
//  CardRequest.swift
//  Banca

import UIKit

protocol FormViewModel {
    func updateForm()
}

protocol CardRequestModel {
    var formIsValid: Bool { get }
    var buttonBackgroundColor: UIColor { get }
    var buttonTitleColor: UIColor { get }
}

struct CardRequestViewModel: CardRequestModel {
    var name: Card.CardName?
    var type: Card.CardType?
    var formIsValid: Bool { return name != nil && type != nil }
    var buttonBackgroundColor: UIColor { return formIsValid ? #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1) : #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1).withAlphaComponent(0.75) }
    var buttonTitleColor: UIColor { return formIsValid ? .white : UIColor(white: 1, alpha: 2/3) }
}
