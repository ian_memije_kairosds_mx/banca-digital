//
//  AuthenticationViewModel.swift
//  Banca

import UIKit

protocol AuthFormViewModel {
    func updateForm()
}

protocol AuthenticationViewModel {
    var formIsValid: Bool { get }
    var buttonBackgroundColor: UIColor { get }
    var buttonTitleColor: UIColor { get }
}

struct LoginViewModel: AuthenticationViewModel {
    var email: String?
    var password: String?
    var formIsValid: Bool { return email?.isEmpty == false && password?.isEmpty == false }
    var buttonBackgroundColor: UIColor { return formIsValid ? #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1) : #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1).withAlphaComponent(0.75) }
    var buttonTitleColor: UIColor { return formIsValid ? .white : UIColor(white: 1, alpha: 2/3) }
}

struct RegistrationViewModel: AuthenticationViewModel {
    var name: String?
    var lastname: String?
    var email: String?
    var password: String?
    var passwordConfirmation: String?
    var formIsValid: Bool { return name?.isEmpty == false && lastname?.isEmpty == false && email?.isEmpty == false && password?.isEmpty == false && passwordConfirmation?.isEmpty == false }
    var buttonBackgroundColor: UIColor { return formIsValid ? #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1) : #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1).withAlphaComponent(0.75) }
    var buttonTitleColor: UIColor { return formIsValid ? .white : UIColor(white: 1, alpha: 2/3) }
}
