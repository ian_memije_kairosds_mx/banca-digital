//
//  Cards.swift
//  Banca

struct Card {
    let id: String
    let name: CardName
    let type: CardType
    
    enum CardName: String {
        case Gold = "Tarjeta Oro"
        case Premium = "Tarjeta Premium"
        case Black = "Tarjeta Black"
        case none = "nil"
    }
    
    enum CardType: String {
        case TDC  = "TDC"
        case TDD = "TDD"
        case none = "nil"
    }
    
    init(dictionary: [String : Any]) {
        self.name = CardName(rawValue: dictionary["name"] as? String ?? "" ) ?? .none
        self.type = CardType(rawValue: dictionary["type"] as? String ?? "") ?? .none
        self.id = dictionary["_id"] as? String ?? ""
    }
}

struct CardInfo {
    var id: String = ""
    var name: String = ""
    var type: String = ""
    var userId: String = ""
    var deposits: Int = 0
    var withdrawals: Int = 0
    var balance: Int = 0
    
    init(dictionary: [String : Any]) {
        self.id = dictionary["_id"] as? String ?? ""
        self.name = dictionary["name"] as? String ?? ""
        self.type = dictionary["type"] as? String ?? ""
        self.userId = dictionary["userId"] as? String ?? ""
        self.deposits = dictionary["deposits"] as? Int ?? 0
        self.withdrawals = dictionary["withdrawals"] as? Int ?? 0
        self.balance = dictionary["balance"] as? Int ?? 0
    }
}

struct CardCellViewModel {
    private let card: Card

    var name: Card.CardName { return card.name }
    var type: Card.CardType { return card.type }
    
    
    init(card: Card) {
        self.card = card
    }
}
