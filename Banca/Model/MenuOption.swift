//
//  MenuOption.swift
//  Banca

import UIKit

enum MenuOption: Int, CustomStringConvertible, CaseIterable {
    case Profile
    case CardRequest
    case Notifications
    case Settings
    
    var description: String {
        switch self {
        case .Profile: return "Perfil"
        case .CardRequest: return "Solicitar Tarjeta"
        case .Notifications: return "Notificaciones"
        case .Settings: return "Configuración"
        }
    }
    
    var image: UIImage {
        switch self {
        case .Profile: return UIImage(systemName: "person.fill")!
        case .CardRequest: return UIImage(systemName: "creditcard.fill")!
        case .Notifications: return UIImage(systemName: "envelope.fill")!
        case .Settings: return UIImage(systemName: "gearshape.fill")!
        }
    }
}
