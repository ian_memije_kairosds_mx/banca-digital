//
//  Constants.swift
//  Banca

import UIKit

public let ENDPOINT_CREATEUSER = "https://kairos-trainees-api.herokuapp.com/api/auth/user/create"
public let ENDPOINT_LOGIN = "https://kairos-trainees-api.herokuapp.com/api/auth/user/authenticate"
public let ENDPOINT_ACCOUNTS = "https://kairos-trainees-api.herokuapp.com/api/accounts"
public let ENDPOINT_CARDS = "https://kairos-trainees-api.herokuapp.com/api/catalogs/cards"

public let CONTENT_TYPE = "application/json"
public let X_ACCESS_TOKEN = "x-access-token"
