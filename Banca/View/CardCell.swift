//
//  CardCell.swift
//  Banca

import UIKit

class CardCell: UITableViewCell {
    //MARK: -Properties
    var viewModel: CardCellViewModel? {
        didSet{ configure() }
    }
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.textColor = .darkGray
        label.textAlignment = .center
        return label
    }()
    
    private let typeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17)
        label.textColor = .darkGray
        label.textAlignment = .center
        return label
    }()
    
    //MARK: -Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let containerView_left = UIView()
        addSubview(containerView_left)
        containerView_left.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, width: self.frame.width / 2, height: self.frame.height)
        let containerView_right = UIView()
        addSubview(containerView_right)
        containerView_right.anchor(top: self.topAnchor, left: containerView_left.rightAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, width: self.frame.width / 2, height: self.frame.height)
        containerView_left.addSubview(nameLabel)
        nameLabel.centerY(inView: containerView_left)
        nameLabel.centerX(inView: containerView_left)
        containerView_right.addSubview(typeLabel)
        typeLabel.centerY(inView: containerView_right)
        typeLabel.centerX(inView: containerView_right)
        self.backgroundColor = .systemGray6
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: -Helpers
    func configure() {
        guard let viewModel = viewModel else { return }

        nameLabel.text = viewModel.name.rawValue
        typeLabel.text = viewModel.type.rawValue
    }
}
