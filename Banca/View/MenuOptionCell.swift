//
//  MenuOptionCell.swift
//  Banca


import UIKit

class MenuOptionCell: UITableViewCell {
    
    //MARK: -Properties
    let iconImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        iv.tintColor = .white
        return iv
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 21)
        return label
    }()

    //MARK: -Lifecycle
override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    configureUI()
}

required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
}

    //MARK: -Actions

    //MARK: -API

    //MARK: -Helpers
    func configureUI() {
       backgroundColor = .darkGray
        selectionStyle = .none
        
        addSubview(iconImageView)
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        iconImageView.centerY(inView: self)
        iconImageView.anchor(left: self.leftAnchor, paddingLeft: 12, width: 24, height: 24)
        addSubview(descriptionLabel)
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.centerY(inView: self)
        descriptionLabel.anchor(left: iconImageView.rightAnchor, paddingLeft: 12, paddingRight: 12)
        
    }
}
