//
//  AuthService.swift
//  Banca

import UIKit
import Alamofire
import JWTDecode
import KeychainSwift

struct UserLogin: Encodable {
    let email: String
    let password: String
}

struct UserRegister: Encodable {
    let email: String
    let firstName: String
    let lastName: String
    let password: String
}

struct availableCards: Encodable {
    let _id: String
    let firstName: String
    let lastName: String
    let password: String
}

public var token = ""
let keychain = KeychainSwift()

struct AuthService {
    static func logUserIn(withUser backUser: UserLogin, completion: @escaping (_ token: String) -> Void) {
        let userParams: Dictionary = ["email" : backUser.email, "password" : backUser.password]
        AF.request(ENDPOINT_LOGIN, method: .post, parameters: userParams, encoder: JSONParameterEncoder.default, headers: [.contentType(CONTENT_TYPE)]).responseJSON { response in
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: response.data!) as? [String : Any] else { return }
                token = stringFromAny(jsonObject["token"])
                completion(token)
            } catch {
                return
            }
        }
    }
    
    static func registerUser(withUser newUser: UserRegister) {
        let newUserParams: Dictionary = ["email" : newUser.email, "firstname" : newUser.firstName, "lastname" : newUser.lastName, "password" : newUser.password]
        AF.request(ENDPOINT_CREATEUSER, method: .post, parameters: newUserParams, encoder: JSONParameterEncoder.default, headers: [.contentType(CONTENT_TYPE)]).validate(statusCode: 200..<299).responseJSON { response in
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: response.data!) as? [String : Any] else { return }
                print("DEBUG: register user - \(stringFromAny(jsonObject["success"]))")
                return
            } catch {
                return
            }
        }.resume()
    }
    
    static func JWTdecode(token: String) -> User {
        var userDecoded: User = User(location: "", token: token, cards: [], cardsInfo: [], id: "0", iat: 0, exp: 0, dictionary: [:])
        var jwt: JWT?
        do {
            jwt = try decode(jwt: token)
        } catch {
            print("DEBUG: Error decoding jwt")
        }
        
        if jwt != nil {
            userDecoded.id = jwt?.body["id"] as! String
            userDecoded.exp = jwt?.body["exp"] as! Int
            userDecoded.iat = jwt?.body["iat"] as! Int
            userDecoded.email = jwt?.body["email"] as! String
            userDecoded.firstName = jwt?.body["firstname"] as! String
            userDecoded.lastName = jwt?.body["lastname"] as! String
            userDecoded.token = token
        }
        return userDecoded
    }
    
    static func getCards(forToken token: String, completion: @escaping([Card], [CardInfo]) -> Void) {
        var cards = [Card]()
        var cardsInfo = [CardInfo]()
        var items: Array<Any>?
        
        let headers: HTTPHeaders = HTTPHeaders(["Content-type" : CONTENT_TYPE, X_ACCESS_TOKEN : token])
        AF.request(ENDPOINT_ACCOUNTS, method: .get, parameters: .none, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<299).responseJSON { response in
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: response.data!) as? [String : Any] else { return }
                items = (jsonObject["response"]).map { ($0 as! Array<Any>) }
            } catch {
                return
            }
            guard let item = items else { return }
            for i in item {
                guard var name = (i as! Dictionary<String, Any>)["name"] else { return }
                guard let type = (i as! Dictionary<String, Any>)["type"] else { return }
                guard let id = (i as! Dictionary<String, Any>)["_id"] else { return }
                guard let balance = (i as! Dictionary<String, Any>)["balance"] else { return }
                guard let deposits = (i as! Dictionary<String, Any>)["deposits"] else { return }
                guard let withdrawals = (i as! Dictionary<String, Any>)["withdrawals"] else { return }
                guard let userId = (i as! Dictionary<String, Any>)["userId"] else { return }
                if name as! String == "Tarjeta de Credito" {
                    name = "Tarjeta de Crédito"
                } else if name as! String == "Tarjeta de Debito" {
                    name = "Tarjeta de Débito"
                }
                cards.append(Card(dictionary: ["name" : name, "type" : type, "_id" : id]))
                cardsInfo.append(CardInfo(dictionary: ["name" : name, "type" : type, "_id" : id, "balance" : balance, "deposits" : deposits, "withdrawals" : withdrawals, "userId" : userId]))
            }
            
            completion(cards, cardsInfo)
        }
    }
    
    static func registerCard(withUserId id: String, withCardName name: String, withCardType type: String, withToken token: String, completion: @escaping(String) -> Void) {
        var cardName: String = ""
        var cardType: String = ""
        
        if type == "Tarjeta de Crédito" {
            cardType = "TDC"
        } else if type == "Tarjeta de Débito" {
            cardType = "TDD"
        }
        cardName = name
        let newCardParams: Dictionary = ["userId" : id, "type" : cardType, "name" : cardName]
        let headers: HTTPHeaders = HTTPHeaders(["Content-type" : CONTENT_TYPE, X_ACCESS_TOKEN : token])
        AF.request(ENDPOINT_ACCOUNTS, method: .post, parameters: newCardParams, encoder: JSONParameterEncoder.default, headers: headers).validate(statusCode: 200..<299).responseJSON { response in
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: response.data!) as? [String : Any] else { return }
                print("DEBUG: register card - \(stringFromAny(jsonObject["success"]))")
                completion("\(stringFromAny(jsonObject["success"]))")
                return
            } catch { return }
        }.resume()
    }
    
    static func getAvailableCards(forToken token: String, completion: @escaping([String]) -> Void) {
        var availableTypeCard = [String]()
        var items: Dictionary<String, Any>?
        
        let headers: HTTPHeaders = HTTPHeaders(["Content-type" : CONTENT_TYPE, X_ACCESS_TOKEN : token])
        AF.request(ENDPOINT_CARDS, method: .get, parameters: .none, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<299).responseJSON { response in
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: response.data!) as? [String : Any] else { return }
                items = (jsonObject["response"]).map { ($0) } as? Dictionary<String, Any>
            } catch {
                return
            }
            guard let item = items else { return }
            var objects = [String : Any]()
            for (key, value) in item {
                objects[key] = value
            }
            guard let object: Array<Dictionary<String, Any>> = objects["type_cards"] as? Array<Dictionary<String, Any>> else { return }
            for i in object {
                availableTypeCard.append(i["type"] as! String)
            }
            completion(availableTypeCard)
        }
    }
    
    static func LogOff(withViewController viewController: UIViewController) {
        removeToken()
        viewController.dismiss(animated: true)
    }
    
    static func removeToken() {
        keychain.clear()
        getLocation()
    }
    
    static func getLocation() {
        LocationManager.shared.getUserLocation { location in
            LocationManager.shared.resolveLocationName(withLocation: location) { locationName in
                guard let locationName = locationName else { return }
                keychain.set(locationName, forKey: "location")
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            guard let _ = keychain.get("location") else {
                print("DEBUG: Acepte el uso de ubicación.")
                return
            }
        }
    }
}
