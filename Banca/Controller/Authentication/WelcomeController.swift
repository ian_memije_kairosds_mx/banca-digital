//
//  WelcomeController.swift
//  Banca

import UIKit
import KeychainSwift

class WelcomeController: UIViewController {
    
    //MARK: -Properties
    private var location: String?
    private let keychain = KeychainSwift()
    
    private let logoImage: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        iv.backgroundColor = .white
        iv.image = #imageLiteral(resourceName: "logo")
        return iv
    }()
    
    private let completeLogoImage: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        iv.backgroundColor = .white
        iv.image = #imageLiteral(resourceName: "logo-text-kairos-white")
        return iv
    }()
    
    private let welcomeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 24)
        label.text = "Bienvenido"
        label.textAlignment = .center
        return label
    }()
    
    private lazy var loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Login", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 21)
        button.setTitleColor(.systemOrange, for: .normal)
        button.addTarget(self, action: #selector(handleLogInButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var registerButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Registro", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 21)
        button.setTitleColor(.darkGray, for: .normal)
        button.addTarget(self, action: #selector(handleRegisterButton), for: .touchUpInside)
        return button
    }()
    
    //MARK: -Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AuthService.getLocation()
        configureUI()
    }
    
    //MARK: -Actions
    
    @objc func handleLogInButton() {
        let controller = LoginController()
        let nav = UINavigationController(rootViewController: controller)
        nav.modalPresentationStyle = .fullScreen
        present(nav, animated: true, completion: nil)
    }
    
    @objc func handleRegisterButton() {
        let controller = RegistrationController()
        let nav = UINavigationController(rootViewController: controller)
        nav.modalPresentationStyle = .fullScreen
        present(nav, animated: true, completion: nil)
    }
    
    //MARK: -API
    
    //MARK: -Helpers
    private func configureUI() {
        view.backgroundColor = .white
        
        navigationController?.navigationBar.isHidden = true
        
        let stackView = UIStackView(arrangedSubviews: [logoImage, completeLogoImage, welcomeLabel])
        logoImage.setHeight(147)
        completeLogoImage.setHeight(36)
        stackView.axis = .vertical
        stackView.spacing = 60
        stackView.distribution = .equalSpacing
        view.addSubview(stackView)
        stackView.centerX(inView: view, topAnchor: view.safeAreaLayoutGuide.topAnchor, paddingTop: 60)
        
        let divider = UIView()
        divider.backgroundColor = .systemOrange
        view.addSubview(loginButton)
        view.addSubview(registerButton)
        view.addSubview(divider)
        
        loginButton.anchor(top: stackView.bottomAnchor, paddingTop: 165)
        registerButton.anchor(top: stackView.bottomAnchor, paddingTop: 165)
        divider.centerX(inView: view)
        divider.anchor(top: stackView.bottomAnchor, left: loginButton.rightAnchor, right: registerButton.leftAnchor, paddingTop: 162, paddingLeft: 21, paddingRight: 21, width: 1, height: 42)
    }
}
