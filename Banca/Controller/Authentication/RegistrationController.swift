//
//  RegistrationController.swift
//  Banca

import UIKit
import KeychainSwift

class RegistrationController: UIViewController {
    
    //MARK: -Properties
    private var viewModel = RegistrationViewModel()
    private let keychain = KeychainSwift()
    
    public var user: User?
    public var token: String?
    
    private let logoImage: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        iv.backgroundColor = .white
        iv.image = #imageLiteral(resourceName: "logo")
        return iv
    }()
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .black
        label.text = "   Nombre(s)"
        label.textAlignment = .left
        return label
    }()
    
    private let nameTextField: UITextField = {
        let tf = CustomTextField(placeholder: "Ingrese su nombre")
        tf.textColor = .darkGray
        tf.isSecureTextEntry = true
        tf.clearButtonMode = .whileEditing
        tf.keyboardType = .emailAddress
        tf.isSecureTextEntry = false
        return tf
    }()
    
    private let lastnameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .black
        label.text = "   Apellidos"
        label.textAlignment = .left
        return label
    }()
    
    private let lastnameField: UITextField = {
        let tf = CustomTextField(placeholder: "Ingrese sus apellidos")
        tf.textColor = .darkGray
        tf.isSecureTextEntry = true
        tf.clearButtonMode = .whileEditing
        tf.keyboardType = .emailAddress
        tf.isSecureTextEntry = false
        return tf
    }()
    
    private let emailLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .black
        label.text = "   Email"
        label.textAlignment = .left
        return label
    }()
    
    private let emailTextField: UITextField = {
        let tf = CustomTextField(placeholder: "Ingrese su correo electrónico")
        tf.textColor = .darkGray
        tf.isSecureTextEntry = true
        tf.clearButtonMode = .whileEditing
        tf.keyboardType = .emailAddress
        tf.isSecureTextEntry = false
        return tf
    }()
    
    private let passwordLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .black
        label.text = "   Contraseña"
        label.textAlignment = .left
        return label
    }()
    
    private let passwordTextField: UITextField = {
        let tf = CustomTextField(placeholder: "Ingrese su contraseña")
        tf.textColor = .darkGray
        tf.isSecureTextEntry = true
        tf.clearButtonMode = .whileEditing
        tf.keyboardType = .emailAddress
        tf.isSecureTextEntry = true
        return tf
    }()
    
    private let passwordConfirmLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .black
        label.text = "   Confirmar contraseña"
        label.textAlignment = .left
        return label
    }()
    
    private let passwordConfirmTextField: UITextField = {
        let tf = CustomTextField(placeholder: "Confirme su contraseña")
        tf.textColor = .darkGray
        tf.isSecureTextEntry = true
        tf.clearButtonMode = .whileEditing
        tf.keyboardType = .emailAddress
        tf.isSecureTextEntry = true
        return tf
    }()
    
    private lazy var registerButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .systemOrange
        button.setTitle("Solicitar", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 24)
        button.addTarget(self, action: #selector(handleRegister), for: .touchUpInside)
        return button
    }()
    
    private lazy var gotoLoginButton: UIButton = {
        let button = UIButton(type: .system)
        button.attributedTitle(firstPart: "Ir a", secondPart: "Login")
        button.addTarget(self, action: #selector(gotoLogin), for: .touchUpInside)
        return button
    }()
    
    //MARK: -Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        configureUI()
        configureNotificationObservers()
    }
    
    //MARK: -Actions
    @objc func handleRegister() {
        self.showLoader(true)
        
        guard let firstname = nameTextField.text else { return }
        guard let lastname = lastnameField.text else { return }
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        guard let passwordConfirmation = passwordConfirmTextField.text else { return }
        
        let newUser: UserRegister = UserRegister(email: email, firstName: firstname, lastName: lastname, password: password)
        let backUser: UserLogin = UserLogin(email: email, password: password)
        
        if password == passwordConfirmation {
            self.showLoader(false)
            AuthService.registerUser(withUser: newUser)
            AuthService.logUserIn(withUser: backUser) { tokenAuth in
                
                self.showLoader(false)
                if tokenAuth.isEmpty == false {
                    self.user?.token = tokenAuth
                    self.token = tokenAuth
                    
                    self.keychain.set(self.token!, forKey: "token")
                    DispatchQueue.main.async {
                        self.showMessage(withTitle: "Éxito", message: "Inicio de sesión correcto.")
                    }
                    let controller = ContainerController()
                    controller.modalPresentationStyle = .fullScreen
                    self.present(controller, animated: true) {
                        RegistrationController().dismiss(animated: true)
                    }
                } else {
                    self.showMessage(withTitle: "Error", message: "Verifique la información ingresada.")
                }
            }
        }
    }
    
    
    @objc func gotoLogin() {
        let controller = LoginController()
        let nav = UINavigationController(rootViewController: controller)
        nav.modalPresentationStyle = .fullScreen
        present(nav, animated: true, completion: nil)
    }
    
    @objc func textDidChange(sender: UITextField) {
        if sender == nameTextField {
            viewModel.name = sender.text
        } else if sender == lastnameField {
            viewModel.lastname = sender.text
        } else if sender == emailTextField {
            viewModel.email = sender.text
        } else if sender == passwordTextField {
            viewModel.password = sender.text
        } else if sender == passwordConfirmTextField {
            viewModel.passwordConfirmation = sender.text
        }
        updateForm()
    }
    
    //MARK: -API
    
    //MARK: -Helpers
    private func configureUI() {
        view.backgroundColor = .systemGray5
        
        navigationController?.navigationBar.isHidden = false
        navigationItem.hidesBackButton = true
        navigationController?.navigationBar.isTranslucent = true
        
        view.addSubview(logoImage)
        logoImage.centerX(inView: view)
        logoImage.backgroundColor = .systemGray5
        logoImage.anchor(top: view.safeAreaLayoutGuide.topAnchor, paddingTop: 0, height: 81)
        
        let stackViewName = UIStackView(arrangedSubviews: [nameLabel, nameTextField])
        stackViewName.axis = .vertical
        stackViewName.spacing = 2
        stackViewName.distribution = .fillProportionally
        view.addSubview(stackViewName)
        stackViewName.centerX(inView: view, topAnchor: logoImage.bottomAnchor, paddingTop: 12)
        stackViewName.anchor(left: view.safeAreaLayoutGuide.leftAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingLeft: 15, paddingRight: 15)
        
        let stackViewLastname = UIStackView(arrangedSubviews: [lastnameLabel, lastnameField])
        stackViewLastname.axis = .vertical
        stackViewLastname.spacing = 2
        stackViewLastname.distribution = .fillProportionally
        view.addSubview(stackViewLastname)
        stackViewLastname.centerX(inView: view, topAnchor: stackViewName.bottomAnchor, paddingTop: 12)
        stackViewLastname.anchor(left: view.safeAreaLayoutGuide.leftAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingLeft: 15, paddingRight: 15)
        
        let stackViewEmail = UIStackView(arrangedSubviews: [emailLabel, emailTextField])
        stackViewEmail.axis = .vertical
        stackViewEmail.spacing = 2
        stackViewEmail.distribution = .fillProportionally
        view.addSubview(stackViewEmail)
        stackViewEmail.centerX(inView: view, topAnchor: stackViewLastname.bottomAnchor, paddingTop: 12)
        stackViewEmail.anchor(left: view.safeAreaLayoutGuide.leftAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingLeft: 15, paddingRight: 15)
        
        let stackViewPassword = UIStackView(arrangedSubviews: [passwordLabel, passwordTextField])
        stackViewPassword.axis = .vertical
        stackViewPassword.spacing = 2
        stackViewPassword.distribution = .fillProportionally
        view.addSubview(stackViewPassword)
        stackViewPassword.centerX(inView: view, topAnchor: stackViewEmail.bottomAnchor, paddingTop: 12)
        stackViewPassword.anchor(left: view.safeAreaLayoutGuide.leftAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingLeft: 15, paddingRight: 15)
        
        let stackViewPasswordConfirm = UIStackView(arrangedSubviews: [passwordConfirmLabel, passwordConfirmTextField])
        stackViewPasswordConfirm.axis = .vertical
        stackViewPasswordConfirm.spacing = 2
        stackViewPasswordConfirm.distribution = .fillProportionally
        view.addSubview(stackViewPasswordConfirm)
        stackViewPasswordConfirm.centerX(inView: view, topAnchor: stackViewPassword.bottomAnchor, paddingTop: 12)
        stackViewPasswordConfirm.anchor(left: view.safeAreaLayoutGuide.leftAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingLeft: 15, paddingRight: 15)
        
        view.addSubview(registerButton)
        registerButton.layer.cornerRadius = 42 / 2
        registerButton.anchor(left: view.safeAreaLayoutGuide.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingLeft: 39, paddingBottom: 21, paddingRight: 39, height: 42)
        
        view.addSubview(gotoLoginButton)
        gotoLoginButton.anchor(bottom: registerButton.topAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingBottom: 42, paddingRight: 39)
        
        updateForm()
    }
    
    private func configureNotificationObservers() {
        nameTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        lastnameField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        emailTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        passwordConfirmTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
    }
}

//MARK: -FormViewModel
extension RegistrationController: AuthFormViewModel {
    func updateForm(){
        registerButton.backgroundColor = viewModel.buttonBackgroundColor
        registerButton.setTitleColor(viewModel.buttonTitleColor, for: .normal)
        registerButton.isEnabled = viewModel.formIsValid
    }
}
