//
//  LoginController.swift
//  Banca

import UIKit
import KeychainSwift

class LoginController: UIViewController {
    
    //MARK: -Properties
    private var viewModel = LoginViewModel()
    private let keychain = KeychainSwift()
    
    public var token: String?
    public var user: User?
    
    private let logoImage: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        iv.backgroundColor = .white
        iv.image = #imageLiteral(resourceName: "logo")
        return iv
    }()
    
    private let emailLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .black
        label.text = "   Email"
        label.textAlignment = .left
        return label
    }()
    
    private let emailTextField: UITextField = {
        let tf = CustomTextField(placeholder: "Ingrese su correo electrónico")
        tf.textColor = .darkGray
        tf.isSecureTextEntry = true
        tf.clearButtonMode = .whileEditing
        tf.keyboardType = .emailAddress
        tf.isSecureTextEntry = false
        return tf
    }()
    
    private let passwordLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .black
        label.text = "   Contraseña"
        label.textAlignment = .left
        return label
    }()
    
    private let passwordTextField: UITextField = {
        let tf = CustomTextField(placeholder: "Ingrese su contraseña")
        tf.textColor = .darkGray
        tf.isSecureTextEntry = true
        tf.clearButtonMode = .whileEditing
        tf.keyboardType = .emailAddress
        tf.isSecureTextEntry = true
        return tf
    }()
    
    private lazy var accessButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .systemOrange
        button.setTitle("Ingresar", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 24)
        button.addTarget(self, action: #selector(handleLogIn), for: .touchUpInside)
        return button
    }()
    
    private lazy var gotoRegisterButton: UIButton = {
        let button = UIButton(type: .system)
        button.attributedTitle(firstPart: "Ir a", secondPart: "Registro")
        button.addTarget(self, action: #selector(gotoRegister), for: .touchUpInside)
        return button
    }()
    
    //MARK: -Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        configureUI()
        configureNotificationObservers()
    }
    
    //MARK: -Actions
    @objc func handleLogIn() {
        self.showLoader(true)
        
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        let userLogIn: UserLogin = UserLogin(email: email, password: password)
        
        AuthService.logUserIn(withUser: userLogIn) { tokenAuth in
            self.showLoader(false)
            if tokenAuth.isEmpty == false  {
                self.user?.token = tokenAuth
                self.token = tokenAuth
                self.keychain.set(self.token!, forKey: "token")
                DispatchQueue.main.async {
                    self.showMessage(withTitle: "Éxito", message: "Inicio de sesión correcto.")
                }
                let controller = ContainerController()
                controller.modalPresentationStyle = .fullScreen
                self.present(controller, animated: true) {
                    LoginController().dismiss(animated: true)
                }
            } else {
                self.showMessage(withTitle: "Error", message: "Verifique la información ingresada.")
            }
        }
    }
    
    @objc func gotoRegister() {
        let controller = RegistrationController()
        let nav = UINavigationController(rootViewController: controller)
        nav.modalPresentationStyle = .fullScreen
        present(nav, animated: true, completion: nil)
    }
    
    @objc func textDidChange(sender: UITextField) {
        if sender == emailTextField {
            viewModel.email = sender.text
        } else if sender == passwordTextField {
            viewModel.password = sender.text
        }
        updateForm()
    }
    
    //MARK: -API
    
    //MARK: -Helpers
    private func configureUI() {
        view.backgroundColor = .systemGray5
        
        navigationController?.navigationBar.isHidden = false
        navigationItem.hidesBackButton = true
        navigationController?.navigationBar.isTranslucent = true
        
        view.addSubview(logoImage)
        logoImage.centerX(inView: view)
        logoImage.backgroundColor = .systemGray5
        logoImage.anchor(top: view.safeAreaLayoutGuide.topAnchor, paddingTop: 0, height: 81)
        
        let stackViewEmail = UIStackView(arrangedSubviews: [emailLabel, emailTextField])
        stackViewEmail.axis = .vertical
        stackViewEmail.spacing = 2
        stackViewEmail.distribution = .fillProportionally
        view.addSubview(stackViewEmail)
        stackViewEmail.centerX(inView: view, topAnchor: logoImage.bottomAnchor, paddingTop: 108)
        stackViewEmail.anchor(left: view.safeAreaLayoutGuide.leftAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingLeft: 15, paddingRight: 15)
        
        let stackViewPassword = UIStackView(arrangedSubviews: [passwordLabel, passwordTextField])
        stackViewPassword.axis = .vertical
        stackViewPassword.spacing = 2
        stackViewPassword.distribution = .fillProportionally
        view.addSubview(stackViewPassword)
        stackViewPassword.centerX(inView: view, topAnchor: stackViewEmail.bottomAnchor, paddingTop: 12)
        stackViewPassword.anchor(left: view.safeAreaLayoutGuide.leftAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingLeft: 15, paddingRight: 15)
        
        view.addSubview(accessButton)
        accessButton.layer.cornerRadius = 42 / 2
        accessButton.anchor(left: view.safeAreaLayoutGuide.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingLeft: 39, paddingBottom: 21, paddingRight: 39, height: 42)
        
        view.addSubview(gotoRegisterButton)
        gotoRegisterButton.anchor(bottom: accessButton.topAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingBottom: 42, paddingRight: 39)
        
        updateForm()
    }
    
    private func configureNotificationObservers() {
        emailTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
    }
}

//MARK: -FormViewModel
extension LoginController: AuthFormViewModel {
    func updateForm(){
        accessButton.backgroundColor = viewModel.buttonBackgroundColor
        accessButton.setTitleColor(viewModel.buttonTitleColor, for: .normal)
        accessButton.isEnabled = viewModel.formIsValid
    }
}
