//
//  MenuController.swift
//  Banca

import UIKit

private let reuseIdentifier = "MenuOptionCell"

class MenuController: UIViewController {
    
    //MARK: -Properties
    private var tableView: UITableView!
    public var delegate: AccountsControllerDelegate?
    
    private let logoImage: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        iv.backgroundColor = .darkGray
        iv.image = #imageLiteral(resourceName: "logo")
        return iv
    }()
    
    private lazy var logoffButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Cerrar Sesión", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 21)
        button.setTitleColor(.lightGray, for: .normal)
        button.addTarget(self, action: #selector(handleLogOff), for: .touchUpInside)
        return button
    }()
    
    //MARK: -Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        configureTableView()
    }
    
    //MARK: -Actions
    @objc func handleLogOff() { logOff() }
    
    //MARK: -API
    private func logOff() { AuthService.LogOff(withViewController: self)  }
    
    //MARK: -Helpers
    private func configureUI() {
        view.backgroundColor = .darkGray
    }
    
    private func configureTableView() {
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(MenuOptionCell.self, forCellReuseIdentifier: reuseIdentifier)
        tableView.alwaysBounceVertical = false
        tableView.alwaysBounceHorizontal = false
        tableView.backgroundColor = .darkGray
        tableView.separatorStyle = .none
        tableView.rowHeight = 80
        
        view.addSubview(logoImage)
        logoImage.setHeight(72)
        logoImage.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, paddingTop: 51, paddingLeft: 9)
        
        view.addSubview(logoffButton)
        logoffButton.anchor(left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, paddingLeft: 21, paddingBottom: 21)
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = true
        tableView.anchor(top: logoImage.bottomAnchor, left: view.leftAnchor, bottom: logoffButton.topAnchor, right: view.rightAnchor, paddingTop: 51, paddingLeft: 9, paddingBottom: 9, paddingRight: 9)
    }
}

//MARK: -UITableViewDelegate
extension MenuController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuOption = MenuOption(rawValue: indexPath.row)
        delegate?.handleMenuToggle(forMenuOption: menuOption)
    }
}

//MARK: -UITableViewDataSource
extension MenuController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MenuOption.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! MenuOptionCell
        let menuOption = MenuOption(rawValue: indexPath.row)
        cell.descriptionLabel.text = menuOption?.description
        cell.iconImageView.image = menuOption?.image
        return cell
    }
}
