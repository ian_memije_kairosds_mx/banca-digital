//
//  ContainerController.swift
//  Banca


import UIKit

class ContainerController: UIViewController {
    
    //MARK: -Properties
    private var menuController: MenuController!
    private var centerController: UIViewController!
    private var isExpanded = false
    
    //MARK: -Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureHomeController()
    }
    
    //MARK: -Actions
    
    //MARK: -API
    
    //MARK: -Helpers
    private  func configureHomeController() {
        let homeController  = AccountsController()
        homeController.delegate = self
        centerController = UINavigationController(rootViewController: homeController)
        view.addSubview(centerController.view)
        addChild(centerController)
        centerController.didMove(toParent: self)
        
    }
    
    private func configureMenuController() {
        if menuController == nil {
            menuController = MenuController()
            menuController.delegate = self
            view.insertSubview(menuController.view, at: 0)
            addChild(menuController)
            menuController.didMove(toParent: self)
        }
    }
    
    private func animateMenu(shouldExpand: Bool, menuOption: MenuOption?) {
        if shouldExpand {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.centerController.view.frame.origin.x = self.centerController.view.frame.width - 80
            }, completion: nil)
        } else {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.centerController.view.frame.origin.x = 0
            }) { _ in
                guard let menuOption = menuOption else { return }
                self.didSelectMenuOption(menuOption: menuOption)
            }
        }
        animateStatusBar()
    }
    
    private func didSelectMenuOption(menuOption: MenuOption) {
        switch menuOption {
        case .Profile:
            let controller = ProfileController()
            let nav = UINavigationController(rootViewController: controller)
            nav.modalPresentationStyle = .popover
            present(nav, animated: true, completion: nil)
            
        case .CardRequest:
            let controller = CardRequestController()
            let nav = UINavigationController(rootViewController: controller)
            nav.modalPresentationStyle = .fullScreen
            present(nav, animated: true, completion: nil)
            
        case .Notifications:
            let controller = NotificationsController()
            let nav = UINavigationController(rootViewController: controller)
            nav.modalPresentationStyle = .fullScreen
            present(nav, animated: true, completion: nil)
            
        case .Settings:
            let controller = SettingsController()
            let nav = UINavigationController(rootViewController: controller)
            nav.modalPresentationStyle = .popover
            present(nav, animated: true, completion: nil)
        }
    }
    
    private func animateStatusBar() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.setNeedsStatusBarAppearanceUpdate()
        }, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .darkContent }
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation { return .slide }
    override var prefersStatusBarHidden: Bool { return isExpanded }
}

//MARK: -AccountsControllerDelegate
extension ContainerController: AccountsControllerDelegate {
    func handleMenuToggle(forMenuOption menuOption: MenuOption?) {
        if !isExpanded {
            configureMenuController()
        }
        isExpanded.toggle()
        animateMenu(shouldExpand: isExpanded, menuOption: menuOption)
    }
}
