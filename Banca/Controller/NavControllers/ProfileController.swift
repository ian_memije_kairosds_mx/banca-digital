//
//  ProfileController.swift
//  Banca

import UIKit

class ProfileController: UIViewController {
    
    //MARK: -Properties
    
    private let profileImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.image = UIImage(systemName: "person.fill")
        iv.tintColor = .darkGray
        return iv
    }()
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.text = "User"
        return label
    }()
    
    private lazy var editProfileButton : UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Editar Perfil", for: .normal)
        button.layer.cornerRadius = 3
        button.layer.borderWidth = 0.5
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(handleEditProfile), for: .touchUpInside)
        return button
    }()
    
    //MARK: -Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)

        configureUI()
    }
    
    //    override func viewWillAppear(_ animated: Bool) {
    //        super.viewWillAppear(animated)
    //    
    //        guard let user = user else { return }
    //        print("DEBUG: Time for session to expire - \((user.exp - Int(Date.now.timeIntervalSince1970)) / (60 * 24)) hours")
    //        if Int(Date.now.timeIntervalSince1970) > user.exp {
    //            AuthService.LogOff(withViewController: self)
    //        }
    //    }
    
    override func viewDidDisappear(_ animated: Bool) {
        view.removeObserver(self, forKeyPath: "contentSize")
    }
    
    //MARK: -Actions
    @objc func handleMenu() {
        dismiss(animated: true)
    }
    
    @objc func handleEditProfile() {
        print("DEBUG: Edit profile")
    }
    
    //MARK: -API
    
    //MARK: -Helpers
    private func configureUI() {
        view.backgroundColor = .systemGray6
        
        view.addSubview(profileImageView)
        profileImageView.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leftAnchor, paddingTop: 15, paddingLeft: 12)
        profileImageView.setDimensions(height: 81, width: 81)
        profileImageView.layer.cornerRadius = 81 / 2
        
        view.addSubview(nameLabel)
        nameLabel.anchor(top: profileImageView.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, paddingTop: 12, paddingLeft: 12)
        
        view.addSubview(editProfileButton)
        editProfileButton.anchor(top: nameLabel.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingTop: 15, paddingLeft: 24, paddingRight: 24)
        
        let divider = UIView()
        divider.backgroundColor = .lightGray
        view.addSubview(divider)
        divider.anchor(top: editProfileButton.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingTop: 12, height: 0.5)
        
        configureNavigationBar()
    }
    
    private func configureNavigationBar() {
        guard let navigationController = navigationController else { return }
        let navigationBar = navigationController.navigationBar
        navigationBar.tintColor = .black
        navigationBar.barStyle = UIBarStyle.default
        navigationBar.prefersLargeTitles = true
        navigationItem.hidesBackButton = true
        navigationItem.title = "Perfil"
        let menuButton = UIBarButtonItem(image: UIImage(systemName: "xmark"), style: .plain, target: self, action: #selector(handleMenu))
        menuButton.tintColor = .darkGray
        navigationItem.leftBarButtonItem = menuButton
    }
}
