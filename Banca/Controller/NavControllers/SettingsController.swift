//
//  SettingsController.swift
//  Banca

import UIKit

class SettingsController: UIViewController {
    
    //MARK: -Properties
    
    //MARK: -Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    
    //    override func viewWillAppear(_ animated: Bool) {
    //        super.viewWillAppear(animated)
    //    
    //        guard let user = user else { return }
    //        print("DEBUG: Time for session to expire - \((user.exp - Int(Date.now.timeIntervalSince1970)) / (60 * 24)) hours")
    //        if Int(Date.now.timeIntervalSince1970) > user.exp {
    //            AuthService.LogOff(withViewController: self)
    //        }
    //    }
    
    //MARK: -Actions
    @objc func handleMenu() {
        dismiss(animated: true)
    }
    
    //MARK: -API
    
    //MARK: -Helpers
    private func configureUI() {
        view.backgroundColor = .white
        
        configureNavigationBar()
    }
    
    private func configureNavigationBar() {
        guard let navigationController = navigationController else { return }
        let navigationBar = navigationController.navigationBar
        navigationBar.tintColor = .black
        navigationBar.barStyle = UIBarStyle.default
        navigationBar.prefersLargeTitles = true
        navigationItem.hidesBackButton = true
        navigationItem.title = "Configuración"
        let menuButton = UIBarButtonItem(image: UIImage(systemName: "xmark"), style: .plain, target: self, action: #selector(handleMenu))
        menuButton.tintColor = .darkGray
        navigationItem.leftBarButtonItem = menuButton
    }
}
