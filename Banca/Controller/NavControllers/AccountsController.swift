//
//  AccountsController.swift
//  Banca

import UIKit
import KeychainSwift

private let reuseIdentifier = "UserCell"

protocol AccountsControllerDelegate { func handleMenuToggle(forMenuOption menuOption: MenuOption?) }

class AccountsController: UIViewController {
    
    //MARK: -Properties
    public var user: User? 
    public var delegate: AccountsControllerDelegate?
    
    private let keychain = KeychainSwift()
    
    private let tableView = UITableView()
    private let containerView = UIView()
    
    private let emailLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = .black
        label.textAlignment = .left
        return label
    }()
    
    private let emptyAccountsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 24)
        label.textColor = .darkGray
        label.textAlignment = .center
        label.text = "No hay cuentas registradas"
        label.numberOfLines = 0
        label.setDimensions(height: 60, width: 210)
        return label
    }()
    
    private let cardNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 24)
        label.textColor = .black
        label.textAlignment = .center
        label.text = "Tarjeta"
        return label
    }()
    
    private let cardTypeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 24)
        label.textColor = .black
        label.textAlignment = .center
        label.text = "Tipo"
        return label
    }()
    
    private let logoImage: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        iv.backgroundColor = .white
        iv.image = #imageLiteral(resourceName: "logo-text-kairos-white")
        return iv
    }()
    
    private let locationLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = .black
        label.textAlignment = .center
        return label
    }()
    
    private var nameLabel: UIBarButtonItem = {
        let label = UIBarButtonItem()
        return label
    }()
    
    //MARK: -Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        user?.token = token
        guard let _ = user else {
            getUser { user in
                self.user = user
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.updateView()
            }
            return
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let user = user else { return }
        print("DEBUG: Time for session to expire - \((Double(user.exp) - Double(Date.now.timeIntervalSince1970)) / (60 * 24)) hours")
        if Int(Date.now.timeIntervalSince1970) > user.exp {
            AuthService.LogOff(withViewController: self)
        }
        getUser { user in
            self.user = user
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.updateView()
        }
    }
    
    //MARK: -Actions
    @objc func handleMenu() {
        delegate?.handleMenuToggle(forMenuOption: nil)
    }
    
    @objc func handleProfile() {
        let controller = ProfileController()
        let nav = UINavigationController(rootViewController: controller)
        nav.modalPresentationStyle = .popover
        present(nav, animated: true, completion: nil)
    }
    
    //MARK: -API
    private func getUser(completion: @escaping(User) -> Void) {
        var loggedToken: String?
        loggedToken = keychain.get("token")
        var userDecoded: User = AuthService.JWTdecode(token: loggedToken!)
        keychain.set(userDecoded.id, forKey: "id")
        AuthService.getCards(forToken: token) { cards, cardsInfo in
            userDecoded.cards = cards
            userDecoded.cardsInfo = cardsInfo
            userDecoded.location = self.keychain.get("location") ?? ""
            
            completion(userDecoded)
        }
    }
    
    //MARK: -Helpers
    private func configureUI() {
        view.backgroundColor = .white
        
        view.addSubview(emailLabel)
        emailLabel.centerX(inView: view)
        emailLabel.backgroundColor = .white
        emailLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leftAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingTop: 9, paddingLeft: 39)
        
        let stackView = UIStackView(arrangedSubviews: [logoImage, locationLabel])
        logoImage.setHeight(42)
        stackView.axis = .vertical
        stackView.spacing = 2
        stackView.distribution = .fillEqually
        view.addSubview(stackView)
        stackView.centerX(inView: view)
        stackView.anchor(left: view.safeAreaLayoutGuide.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingBottom: 21)
        
        containerView.backgroundColor = .systemGray6
        view.addSubview(containerView)
        containerView.anchor(top: emailLabel.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: stackView.topAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingTop: 15, paddingLeft: 39, paddingBottom: 21, paddingRight: 39)
        
        containerView.addSubview(emptyAccountsLabel)
        emptyAccountsLabel.centerX(inView: containerView)
        emptyAccountsLabel.centerY(inView: containerView)
        
        configureNavigationBar()
    }
    
    private func configureNavigationBar() {
        guard let navigationController = navigationController else { return }
        let navigationBar = navigationController.navigationBar
        navigationBar.tintColor = .black
        navigationBar.barStyle = UIBarStyle.default
        navigationBar.prefersLargeTitles = true
        navigationItem.hidesBackButton = true
        navigationItem.title = "Mis Cuentas"
        let menuButton = UIBarButtonItem(image: UIImage(systemName: "line.3.horizontal")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleMenu))
        navigationItem.leftBarButtonItem = menuButton
        let welcomeLabel = UIBarButtonItem(title: "Bienvenido: ", style: .plain, target: self, action: #selector(handleProfile))
        nameLabel = UIBarButtonItem(title: "Usuario", style: .plain, target: self, action: #selector(handleProfile))
        navigationItem.rightBarButtonItems = [nameLabel, welcomeLabel]
    }
    
    private func configureTableView(containerView: UIView) {
        emptyAccountsLabel.isHidden = true
        let verticalDivider = UIView()
        let horizontalDivider = UIView()
        verticalDivider.backgroundColor = .darkGray
        horizontalDivider.backgroundColor = .darkGray
        containerView.addSubview(verticalDivider)
        containerView.addSubview(horizontalDivider)
        containerView.addSubview(cardNameLabel)
        containerView.addSubview(cardTypeLabel)
        verticalDivider.centerX(inView: view)
        horizontalDivider.centerX(inView: containerView)
        verticalDivider.anchor(top: containerView.topAnchor, paddingTop: 15, width: 1, height: 45)
        cardNameLabel.anchor(top: containerView.topAnchor, right: verticalDivider.leftAnchor, paddingTop: 27, paddingRight: 54)
        cardTypeLabel.anchor(top: containerView.topAnchor, left: verticalDivider.rightAnchor, paddingTop: 27, paddingLeft: 54)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CardCell.self, forCellReuseIdentifier: reuseIdentifier)
        tableView.rowHeight = 54
        tableView.backgroundColor = .systemGray6
        
        view.addSubview(tableView)
        tableView.anchor(top: containerView.topAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, paddingTop: 66)
        
        horizontalDivider.anchor(left: containerView.leftAnchor, bottom: tableView.topAnchor, right: containerView.rightAnchor, paddingLeft: 8, paddingBottom: 0, paddingRight: 8, height: 1)
    }
    
    private func updateCards(cards: [Card], containerView: UIView) {
        if cards.isEmpty != true {
            configureTableView(containerView: containerView)
        } else {
            emptyAccountsLabel.isHidden = false
        }
    }
    
    private func updateView() {
        guard let user = user else { return }
        tableView.reloadData()
        emailLabel.text = user.email
        updateCards(cards: user.cards, containerView: containerView)
        locationLabel.text = "\(user.location)"
        nameLabel.title = "\(user.firstName) \(user.lastName)"
    }
}

//MARK: -UITableViewDataSource
extension AccountsController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! CardCell
        let card = user?.cards[indexPath.row]
        cell.viewModel = CardCellViewModel(card: card!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (user?.cards.count)!
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

//MARK: -UITableViewDelegate
extension AccountsController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let user = user else { return }
        if user.cards[indexPath.row].type == .TDD {
            DispatchQueue.main.async {
                self.showMessage(withTitle: "Saldo", message: "$\(Int.random(in: 0..<100))")
            }
        } else if user.cards[indexPath.row].type == .TDC {
            DispatchQueue.main.async {
                self.showMessage(withTitle: "Crédito Disponible", message: "$\(Int.random(in: 0..<100))")
            }
        }
    }
        
    private func handleMarkFavourite(indexPath: IndexPath) {
        print("DEBUG: Mark as favourite - \(indexPath.row)")
        tableView.reloadData()
    }
    
    private func handleMoveToTrash(indexPath: IndexPath) {
        print("DEBUG: Delete card - \(indexPath.row)")
        user?.cards.remove(at: indexPath.row)
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let favorite = UIContextualAction(style: .normal, title: "Favorito") { [weak self] (action, view, completionHandler) in self?.handleMarkFavourite(indexPath: indexPath)
            completionHandler(true)
        }
        favorite.backgroundColor = .systemBlue
        
        return UISwipeActionsConfiguration(actions: [favorite])
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Eliminar") { [weak self] (action, view, completionHandler) in self?.handleMoveToTrash(indexPath: indexPath)
            completionHandler(true)
        }
        delete.backgroundColor = .systemRed
        
        return UISwipeActionsConfiguration(actions: [delete])
    }
}
