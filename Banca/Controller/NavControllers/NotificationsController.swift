//
//  NotificationsController.swift
//  Banca

import UIKit
import KeychainSwift

class NotificationsController: UIViewController {
    
    //MARK: -Properties
    private var user: User?
    private let keychain = KeychainSwift()
    
    private var nameLabel: UIBarButtonItem = {
        let label = UIBarButtonItem()
        return label
    }()
    
    //MARK: -Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        guard let _ = user else {
            getUser { user in
                self.user = user
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.750) {
                self.updateView()
            }
            return
        }
    }
    
    //    override func viewWillAppear(_ animated: Bool) {
    //        super.viewWillAppear(animated)
    //    
    //        guard let user = user else { return }
    //        print("DEBUG: Time for session to expire - \((user.exp - Int(Date.now.timeIntervalSince1970)) / (60 * 24)) hours")
    //        if Int(Date.now.timeIntervalSince1970) > user.exp {
    //            AuthService.LogOff(withViewController: self)
    //        }
    //    }
    
    //MARK: -Actions
    @objc func handleMenu() {
        dismiss(animated: true)
    }
    
    @objc func handleProfile() {
        let controller = ProfileController()
        let nav = UINavigationController(rootViewController: controller)
        nav.modalPresentationStyle = .popover
        present(nav, animated: true, completion: nil)
    }
    
    //MARK: -API
    private func getUser(completion: @escaping(User) -> Void) {
        var loggedToken: String?
        loggedToken = keychain.get("token")
        var userDecoded: User = AuthService.JWTdecode(token: loggedToken!)
        keychain.set(userDecoded.id, forKey: "id")
        guard let location = keychain.get("location") else { return }
        userDecoded.location = location
        completion(userDecoded)
    }
    
    //MARK: -Helpers
    private  func configureUI() {
        view.backgroundColor = .white
        
        configureNavigationBar()
    }
    
    private func configureNavigationBar() {
        guard let navigationController = navigationController else { return }
        let navigationBar = navigationController.navigationBar
        navigationBar.tintColor = .black
        navigationBar.barStyle = UIBarStyle.default
        navigationBar.prefersLargeTitles = true
        navigationItem.hidesBackButton = true
        navigationItem.title = "Notificaciones"
        let menuButton = UIBarButtonItem(image: UIImage(systemName: "xmark"), style: .plain, target: self, action: #selector(handleMenu))
        navigationItem.leftBarButtonItem = menuButton
        nameLabel = UIBarButtonItem(title: "Usuario", style: .plain, target: self, action: #selector(handleProfile))
        navigationItem.rightBarButtonItems = [nameLabel]
    }
    
    private func updateView() {
        guard let user = user else { return }
        nameLabel.title = "\(user.firstName) \(user.lastName)"
    }
}
