//
//  CardRequestController.swift
//  Banca

import UIKit
import DropDown
import KeychainSwift

class CardRequestController: UIViewController {
    
    //MARK: -Properties
    private var viewModel = CardRequestViewModel()
    private let keychain = KeychainSwift()
    
    private var user: User?
    private var cardName: String?
    private var cardType: String?
    
    private let topUI = UIView()
    private let bottomUI = UIView()
    private let topDropDown = DropDown()
    private let bottomDropDown = DropDown()
    private let containerView = UIView()
    
    private let emailLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = .black
        label.textAlignment = .left
        return label
    }()
    
    private let newCardLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.text = "Selecciona Tarjeta"
        label.setWidth(147)
        label.textColor = .darkGray
        return label
    }()
    
    private lazy var newCardButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "chevron.down"), for: .normal)
        button.tintColor = .darkGray
        button.contentMode = .scaleAspectFit
        button.setWidth(24)
        button.addTarget(self, action: #selector(handleNewCard), for: .touchUpInside)
        return button
    }()
    
    private let newTypeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.text = "Selecciona tipo de Tarjeta"
        label.setWidth(147)
        label.textColor = .darkGray
        return label
    }()
    
    private lazy var newTypeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "chevron.down"), for: .normal)
        button.tintColor = .darkGray
        button.contentMode = .scaleAspectFit
        button.setWidth(24)
        button.addTarget(self, action: #selector(handleNewType), for: .touchUpInside)
        return button
    }()
    
    private let logoImage: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        iv.backgroundColor = .white
        iv.image = #imageLiteral(resourceName: "logo-text-kairos-white")
        return iv
    }()
    
    private let locationLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = .black
        label.textAlignment = .center
        return label
    }()
    
    private lazy var requestButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .systemOrange
        button.setTitle("Ingresar", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 24)
        button.addTarget(self, action: #selector(handleRequest), for: .touchUpInside)
        return button
    }()
    
    private var nameLabel: UIBarButtonItem = {
        let label = UIBarButtonItem()
        return label
    }()
    
    //MARK: -Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        guard let _ = user else {
            getUser { user in
                self.user = user
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.750) {
                self.updateView()
            }
            updateForm()
            return
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let user = user else { return }
        if Int(Date.now.timeIntervalSince1970) > user.exp {
            AuthService.LogOff(withViewController: self)
        }
    }
    
    //MARK: -Actions
    @objc func handleMenu() {
        dismiss(animated: true)
    }
    
    @objc func handleNewCard() {
        self.newCardButton.setImage(UIImage(systemName: "chevron.up"), for: .normal)
        self.topDropDown.dataSource = ["Tarjeta Oro", "Tarjeta Premium", "Tarjeta Black"]
        self.topDropDown.anchorView = self.topUI
        self.topDropDown.bottomOffset = CGPoint(x: 0, y: self.newCardButton.frame.size.height)
        self.topDropDown.show()
        self.topDropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let _ = self else { return }
            self?.newCardLabel.text = item
            self?.cardName = item
            self?.newCardButton.setImage(UIImage(systemName: "chevron.down"), for: .normal)
            var updateName = Card.CardName.none
            if item == "Tarjeta Oro" {
                updateName = .Gold
            } else if item == "Tarjeta Premium" {
                updateName = .Premium
            } else if item == "Tarjeta Black" {
                updateName = .Black
            } else {
                updateName = .none
            }
            self?.viewModel.name = updateName
            self?.updateForm()
        }
    }
    
    @objc func handleNewType() {
        var type = [String]()
        AuthService.getAvailableCards(forToken: token) {types in
            for i in 0...(types.count - 1) {
                if types[i] == "TDD" {
                    type.append("Tarjeta de Débito")
                } else if types[i] == "TDC" {
                    type.append("Tarjeta de Crédito")
                }
            }
            self.newTypeButton.setImage(UIImage(systemName: "chevron.up"), for: .normal)
            self.bottomDropDown.dataSource = type
            self.bottomDropDown.anchorView = self.bottomUI
            self.bottomDropDown.bottomOffset = CGPoint(x: 0, y: self.newTypeButton.frame.size.height)
            self.bottomDropDown.show()
            self.bottomDropDown.selectionAction = { [weak self] (index: Int, item: String) in
                guard let _ = self else { return }
                self?.newTypeLabel.text = item
                self?.cardType = item
                self?.newTypeButton.setImage(UIImage(systemName: "chevron.down"), for: .normal)
                var updateName = Card.CardType.none
                if item == "Tarjeta de Débito" {
                    updateName = .TDD
                } else if item == "Tarjeta de Crédito" {
                    updateName = .TDC
                }
                self?.viewModel.type = updateName
                self?.updateForm()
            }
        }
    }
    
    @objc func handleRequest() {
        guard let cardName = cardName else {
            self.showMessage(withTitle: "Error", message: "Verifique la información ingresada.")
            return
        }
        guard let cardType = cardType else {
            self.showMessage(withTitle: "Error", message: "Verifique la información ingresada.")
            return
        }
        guard let user = user else { return }
        AuthService.registerCard(withUserId: user.id, withCardName: cardName, withCardType: cardType, withToken: token) { message in
            DispatchQueue.main.async {
                self.showMessage(withTitle: "Éxito", message: message)
            }
        }
    }
    
    @objc func handleProfile() {
        let controller = ProfileController()
        let nav = UINavigationController(rootViewController: controller)
        nav.modalPresentationStyle = .popover
        present(nav, animated: true, completion: nil)
    }
    
    //MARK: -API
    private func getUser(completion: @escaping(User) -> Void) {
        var loggedToken: String?
        loggedToken = keychain.get("token")
        var userDecoded: User = AuthService.JWTdecode(token: loggedToken!)
        keychain.set(userDecoded.id, forKey: "id")
        guard let location = keychain.get("location") else { return }
        userDecoded.location = location
        completion(userDecoded)
    }
    
    //MARK: -Helpers
    private func configureUI() {
        view.backgroundColor = .white
        
        view.addSubview(emailLabel)
        emailLabel.centerX(inView: view)
        emailLabel.backgroundColor = .white
        emailLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leftAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingTop: 9, paddingLeft: 39)
        
        let stackView = UIStackView(arrangedSubviews: [logoImage, locationLabel])
        logoImage.setHeight(42)
        stackView.axis = .vertical
        stackView.spacing = 2
        stackView.distribution = .fillEqually
        view.addSubview(stackView)
        stackView.centerX(inView: view)
        stackView.anchor(left: view.safeAreaLayoutGuide.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingBottom: 21)
        
        containerView.backgroundColor = .systemGray6
        view.addSubview(containerView)
        containerView.anchor(top: emailLabel.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: stackView.topAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingTop: 15, paddingLeft: 39, paddingBottom: 21, paddingRight: 39)
        
        containerView.addSubview(requestButton)
        requestButton.layer.cornerRadius = 42 / 2
        requestButton.anchor(left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, paddingLeft: 9, paddingBottom: 21, paddingRight: 9, height: 42)
        
        
        let buttonsStackView = UIStackView(arrangedSubviews: [topUI, bottomUI])
        containerView.addSubview(buttonsStackView)
        buttonsStackView.centerX(inView: containerView)
        topUI.backgroundColor = .systemGray5
        bottomUI.backgroundColor = .systemGray5
        buttonsStackView.axis = .vertical
        buttonsStackView.spacing = 24
        buttonsStackView.distribution = .fillEqually
        buttonsStackView.anchor(top: containerView.topAnchor, left: containerView.leftAnchor, right: containerView.rightAnchor, paddingTop: 21, paddingLeft: 9, paddingRight: 9)
        
        let topStackView = UIStackView(arrangedSubviews: [newCardLabel, newCardButton])
        topUI.addSubview(topStackView)
        topStackView.centerX(inView: topUI)
        topStackView.axis = .horizontal
        topStackView.spacing = 12
        topStackView.distribution = .fill
        newCardButton.setHeight(topStackView.frame.height)
        topStackView.anchor(top: topUI.topAnchor, left: topUI.leftAnchor, bottom: topUI.bottomAnchor, right: topUI.rightAnchor, paddingTop: 9, paddingLeft: 9, paddingBottom: 9, paddingRight: 9, height: 60)
        topUI.layer.cornerRadius = 60 / 2
        
        let bottomStackView = UIStackView(arrangedSubviews: [newTypeLabel, newTypeButton])
        bottomUI.addSubview(bottomStackView)
        bottomStackView.centerX(inView: bottomUI)
        bottomStackView.axis = .horizontal
        bottomStackView.spacing = 12
        bottomStackView.distribution = .fill
        newTypeButton.setHeight(topStackView.frame.height)
        bottomStackView.anchor(top: bottomUI.topAnchor, left: bottomUI.leftAnchor, bottom: bottomUI.bottomAnchor, right: bottomUI.rightAnchor, paddingTop: 9, paddingLeft: 9, paddingBottom: 9, paddingRight: 9, height: 60)
        bottomUI.layer.cornerRadius = 60 / 2
        
        configureNavigationBar()
    }
    
    private func configureNavigationBar() {
        guard let navigationController = navigationController else { return }
        let navigationBar = navigationController.navigationBar
        navigationBar.tintColor = .black
        navigationBar.barStyle = UIBarStyle.default
        navigationBar.prefersLargeTitles = true
        navigationItem.hidesBackButton = true
        navigationItem.title = "Solicitar Tarjeta"
        let menuButton = UIBarButtonItem(image: UIImage(systemName: "xmark"), style: .plain, target: self, action: #selector(handleMenu))
        navigationItem.leftBarButtonItem = menuButton
        let welcomeLabel = UIBarButtonItem(title: "Bienvenido: ", style: .plain, target: self, action: #selector(handleProfile))
        nameLabel = UIBarButtonItem(title: "Usuario", style: .plain, target: self, action: #selector(handleProfile))
        navigationItem.rightBarButtonItems = [nameLabel, welcomeLabel]
    }
    
    private func updateView() {
        guard let user = user else { return }
        emailLabel.text = user.email
        locationLabel.text = "\(user.location)"
        nameLabel.title = "\(user.firstName) \(user.lastName)"
    }
}

//MARK: -FormViewModel
extension CardRequestController: FormViewModel {
    func updateForm(){
        requestButton.backgroundColor = viewModel.buttonBackgroundColor
        requestButton.setTitleColor(viewModel.buttonTitleColor, for: .normal)
        requestButton.isEnabled = viewModel.formIsValid
    }
}
